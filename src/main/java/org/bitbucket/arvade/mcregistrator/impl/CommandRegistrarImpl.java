package org.bitbucket.arvade.mcregistrator.impl;

import com.google.inject.Inject;
import com.google.inject.Injector;
import org.bitbucket.arvade.mcregistrator.CommandRegistrar;
import org.bitbucket.arvade.mcregistrator.NamedCommandExecutor;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;

public class CommandRegistrarImpl implements CommandRegistrar {

    private PackageScanner packageScanner;
    private JavaPlugin javaPlugin;
    private Injector injector;

    @Inject
    public CommandRegistrarImpl(PackageScanner packageScanner, JavaPlugin javaPlugin, Injector injector) {
        this.packageScanner = packageScanner;
        this.javaPlugin = javaPlugin;
        this.injector = injector;
    }

    @Override
    public void registerCommands(String packagePath) {
        Collection<Class<?>> classes = packageScanner.findClasses(packagePath);

        classes.stream()
                .filter(NamedCommandExecutor.class::isAssignableFrom)
                .map(aClass -> injector.getInstance(aClass))
                .map(o -> (NamedCommandExecutor) o)
                .forEach(namedCommandExecutor -> javaPlugin.getCommand(namedCommandExecutor.getName()).setExecutor(namedCommandExecutor));
    }
}
