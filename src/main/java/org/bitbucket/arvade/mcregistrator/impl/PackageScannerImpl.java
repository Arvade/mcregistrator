package org.bitbucket.arvade.mcregistrator.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public class PackageScannerImpl implements PackageScanner {

    private JavaPlugin javaPlugin;

    @Inject
    public PackageScannerImpl(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
    }

    public Collection<Class<?>> findClasses(String packageName) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(this.javaPlugin.getClass().getClassLoader()))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(packageName))));

        return reflections.getSubTypesOf(Object.class);
    }
}
