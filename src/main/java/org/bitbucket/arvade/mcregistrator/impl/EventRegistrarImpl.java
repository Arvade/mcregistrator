package org.bitbucket.arvade.mcregistrator.impl;

import com.google.inject.Inject;
import com.google.inject.Injector;
import org.bitbucket.arvade.mcregistrator.EventRegistrar;
import org.bitbucket.arvade.mcregistrator.PackageScanner;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;
import java.util.List;

public class EventRegistrarImpl implements EventRegistrar {

    private PackageScanner packageScanner;
    private JavaPlugin javaPlugin;
    private Injector injector;

    @Inject
    public EventRegistrarImpl(PackageScanner packageScanner, JavaPlugin javaPlugin, Injector injector) {
        this.packageScanner = packageScanner;
        this.javaPlugin = javaPlugin;
        this.injector = injector;
    }

    @Override
    public void registerEvents(String packagePath) {
        Collection<Class<?>> classes = packageScanner.findClasses(packagePath);

        classes.stream()
                .filter(Listener.class::isAssignableFrom)
                .map(aClass -> injector.getInstance(aClass))
                .map(o -> (Listener) o)
                .forEach(Listener -> javaPlugin.getServer().getPluginManager().registerEvents(Listener, javaPlugin));
    }
}
