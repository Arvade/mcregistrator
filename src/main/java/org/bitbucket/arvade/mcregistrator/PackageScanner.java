package org.bitbucket.arvade.mcregistrator;

import java.util.Collection;

public interface PackageScanner {
    Collection<Class<?>> findClasses(String packageName);
}
