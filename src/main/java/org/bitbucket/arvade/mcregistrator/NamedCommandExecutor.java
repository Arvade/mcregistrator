package org.bitbucket.arvade.mcregistrator;

import org.bukkit.command.CommandExecutor;

public interface NamedCommandExecutor extends CommandExecutor {
    String getName();
}
