package org.bitbucket.arvade.mcregistrator;

public interface EventRegistrar {
    void registerEvents(String packagePath);
}
