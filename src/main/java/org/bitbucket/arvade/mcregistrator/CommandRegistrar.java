package org.bitbucket.arvade.mcregistrator;

public interface CommandRegistrar {
    void registerCommands(String packagePath);
}
